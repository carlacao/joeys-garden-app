#!/bin/bash

freqs_manifest=(
	"84.82" "99.42" "164.81" "169.64" "190.42" "213.74" "269.29" "397.70"
	"459.48" "486.80" "570.61" "659.26" "718.92" "739.99" "1637.40"
	"1685.38" "4126.00")

amps_manifest=(
	"-13" "-15" "-15" "-15" "-28" "-15" "-15"
	"-19" "-17" "-22" "-22" "-23" "-23" "-24" "-27"
	"-27" "-27")

function make_tone {
	sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
	echo "sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >>log
}


for ((i = 0; i < ${#freqs_manifest[@]}; i++)); do
	if [ ${freqs_manifest[i]} = "459.48" ]; then
		make_tone triangle
	elif [ ${freqs_manifest[i]} = "190.42" ]; then
		make_tone square
	else
		make_tone sine
	fi
done

files=($(ls -tr *.wav))

pans_manifest=("100.00"
	"3.00"
	"94.00"
	"9.00"
	"88.00"
	"15.00"
	"82.00"
	"21.00"
	"76.00"
	"26.00"
	"71.00"
	"32.00"
	"65.00"
	"38.00"
	"59.00"
	"44.00"
	"53.00"
)

for ((i = 0; i < ${#files[@]}; i++)); do
	ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
	echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >>log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

