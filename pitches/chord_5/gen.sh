#!/bin/bash

freqs=(
    "102.34"
    "220.00"
    "344.22"
    "659.26"
    "739.99"
    "795.39"
    "905.79"
    "2525.22"
    "2917.52"
    "2959.96"
)

amps=(
    "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-30" "-30" "-30"
)

rm log
touch log
echo "SOX LOG" >> log

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs[i]} | tr . -`-$1.wav synth 180 $1 ${freqs[i]} fade 0.5 -0 1 gain ${amps[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs[i]} | tr . -`-$1.wav synth 180 $1 ${freqs[i]} fade 0.5 -0 1 gain ${amps[i]}" >> log
}


for ((i=0;i<${#freqs[@]};i++))
do
    make_tone sine
done

files=($(ls -tr *.wav))

pans=("100.00" "5.00" "90.00" "15.00" "80.00" "25.00" "70.00" "35.00" "60.00" "45.00")

echo "ECASOUND LOG" >> log

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox -V panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done

