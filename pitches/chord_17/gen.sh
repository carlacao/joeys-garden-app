#!/bin/bash

freqs=(
    "59.98"
    "71.33"
    "80.06"
    "95.21"
    "114.87"
    "121.70"
    "169.64"
    "226.45"
    "329.63"
    "472.94"
    "546.42"
    "678.57"
    "783.99"
    "2154.33"
    "2959.96"
    "3227.85"
    "3322.44"
)

amps=(
  "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
  "-22" "-22" "-22" "-22" "-32" "-32" "-28" "-28" "-28"
)

rm log
touch log
echo "SOX LOG"

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs[i]} | tr . -`-$1.wav synth 180 $1 ${freqs[i]} fade 0.5 -0 1 gain ${amps[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs[i]} | tr . -`-$1.wav synth 180 $1 ${freqs[i]} fade 0.5 -0 1 gain ${amps[i]}" >> log
}

for ((i=0;i<${#freqs[@]};i++))
do
  if [ ${freqs[i]} = "783.99" ] || [ ${freqs[i]} = "2154.33" ]
  then
    make_tone triangle
  else
    make_tone sine
  fi
done

files=($(ls -tr *.wav))

pans=("100.00" "4.00" "92.00" "12.00" "84.00" "19.00" "78.00" "25.00" "72.00" "31.00" "66.00" "37.00" "60.00" "43.00" "54.00" "48.00" "50.00")

echo -e "\nECASOUND LOG"

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done

