#!/bin/bash

freqs_manifest=(
	"185.00"
	"246.94"
	"339.29"
	"421.34"
	"433.69"
	"472.94"
	"530.86"
	"659.26"
	"1209.08"
	"1398.91"
	"1637.40"
	"2123.45"
	"2959.96"
	"3003.01"
	"3046.69"
	"3623.14"
)

amps_manifest=(
	"-22" "-38" "-22" "-22" "-22" "-22" "-22"
	"-22" "-22" "-22" "-22" "-22" "-22" "-40" "-40" "-40")

function make_tone {
	sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
	echo "sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >>log
}

rm log
touch log

for ((i = 0; i < ${#freqs_manifest[@]}; i++)); do
	if [ ${freqs_manifest[i]} = "246.94" ] || [ ${freqs_manifest[i]} = "3623.14" ]; then
		make_tone triangle
	elif [ ${freqs_manifest[i]} = "3003.01" ] || [ ${freqs_manifest[i]} = "3046.69" ]; then
		make_tone square
	else
		make_tone sine
	fi
done

files=($(ls -tr *.wav))

pans_manifest=("0.00" "96.00" "8.00" "88.00" "16.00" "80.00" "24.00" "72.00" "31.00" "66.00" "37.00" "61.00" "41.00" "58.00" "43.00" "56.00")

for ((i = 0; i < ${#files[@]}; i++)); do
	ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
	echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >>log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

