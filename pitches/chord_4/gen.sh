#!/bin/bash

freqs_manifest=(
	"138.59"
	"169.64"
	"207.65"
	"320.24"
	"339.29"
	"515.75"
	"772.75"
	"932.33"
	"1157.82"
	"1280.97"
	"1357.15"
	"1437.85"
	"1479.98"
	"1637.40"
	"2154.33"
	"2959.96"
	"3419.79"
)

amps_manifest=(
	"-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
	"-22" "-22" "-22" "-22" "-22" "-22" "-29"
	"-29" "-29")

function make_tone {
	sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
	echo "sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >>log
}


rm log
touch log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
    make_tone sine
done

files=($(ls -tr *.wav))

pans_manifest=("100.00" "4.00" "92.00" "12.00" "84.00" "19.00" "78.00" "24.00" "74.00" "28.00" "70.00" "32.00" "66.00" "36.00" "62.00" "40.00" "58.00")

for ((i = 0; i < ${#files[@]}; i++)); do
	ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
	echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >>log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox -V panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

