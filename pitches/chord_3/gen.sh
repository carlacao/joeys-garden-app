#!/bin/bash

freqs_manifest=(
    "75.57"
    "82.41"
    "92.50"
    "142.65"
    "160.12"
    "196.00"
    "339.29"
    "354.31"
    "562.43"
    "659.26"
    "880.00"
    "932.33"
    "1031.50"
    "1613.93"
)


amps_manifest=(
    "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
    "-22" "-22" "-22" "-22" "-22" "-22" )

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}

rm log
touch log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
    make_tone sine
done

files=($(ls -tr *.wav))

pans_manifest=(
"0.00"
"96.00"
"8.00"
"88.00"
"16.00"
"80.00"
"24.00"
"72.00"
"32.00"
"64.00"
"40.00"
"56.00"
"48.00"
"50.00"
)

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox -V panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done
    
