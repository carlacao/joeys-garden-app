#!/bin/bash

freqs_manifest=(
    "105.34"
    "169.64"
    "201.74"
    "289.45"
    "472.94"
    "604.54"
    "659.26"
    "698.46"
    "739.99"
    "1637.40"
    "2004.27"
    "2004.27"
    "2917.52"
    "2959.96"
    "3838.59"
)

amps_manifest=(
	"-38" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
	"-22" "-22" "-22" "-22" "-22" "-30" "-30"
	"-30" "-30")

function make_tone {
	sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
	echo "sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >>log
}


for ((i = 0; i < ${#freqs_manifest[@]}; i++)); do
	if [ ${freqs_manifest[i]} = "105.34" ]; then
		make_tone square
	else
		make_tone sine
	fi
done

files=($(ls -tr *.wav))

pans_manifest=("0.00" "96.00" "8.00" "90.00" "11.00" "86.00" "17.00" "80.00" "22.00" "76.00" "26.00" "71.00" "33.00" "63.00" "43.00")

for ((i = 0; i < ${#files[@]}; i++)); do
	ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
	echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >>log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox -V panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox -V ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

