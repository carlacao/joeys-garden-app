#!/bin/bash

freqs_manifest=(
"61.74" "64.47" "68.30" "71.33" "89.87" "108.42" "111.60" "169.64" "213.74" "421.34" "472.94" "854.95" "1174.66" "1785.60" "2959.96" "3520.00")

amps_manifest=(
  "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
  "-22" "-22" "-22" "-22" "-22" "-22" "-22"
"-27" "-27"
)

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}


rm log
touch log
echo -e "\n SOX LOG \n" >> log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
    make_tone sine
done


files=($(ls -tr *.wav))

pans_manifest=(
"0.00"
"95.00"
"10.00"
"85.00"
"19.00"
"77.00"
"27.00"
"69.00"
"33.00"
"64.00"
"39.00"
"59.00"
"43.00"
"55.00"
"47.00"
"51.00"
)

echo -e "\n ECASOUND LOG \n" >> log

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done

