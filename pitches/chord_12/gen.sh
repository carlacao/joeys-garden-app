#!/bin/bash

freqs_manifest=(
    "71.33"
    "80.06"
    "87.31"
    "89.87"
    "111.60"
    "134.65"
    "160.12"
    "261.63"
    "421.34"
    "622.25"
    "932.33"
    "987.77"
    "1141.22"
    "1174.66"
    "2959.96"
    "3838.59"
)

amps_manifest=(
  "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
  "-22" "-22" "-28" "-22" "-22" "-22"
  )

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}

if [[ -f log ]]
then
  echo "removing log"
  rm log
fi

touch log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
  if [ ${freqs_manifest[i]} = "1141.22" ]
  then
    make_tone triangle
  else
    make_tone sine
  fi
done

files=($(ls -tr *.wav))

pans_manifest=(
"100.00"
"4.00"
"92.00"
"12.00"
"84.00"
"20.00"
"76.00"
"28.00"
"68.00"
"34.00"
"64.00"
"38.00"
"60.00"
"42.00"
"56.00"
"46.00"
)

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done

