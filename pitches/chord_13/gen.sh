#!/bin/bash

freqs_manifest=(
	"174.61"
	"179.73"
	"339.29"
	"386.37"
	"1209.08"
	"1567.98"
	"1637.40"
	"2093.00"
	"2282.44"
	"2315.64"
	"3729.31"
	"3894.42"
)

amps_manifest=(
	"-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
	"-30" "-30" "-30" "-30")

function make_tone {
	sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
	echo "sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >>log
}

rm log
touch log
echo "SOX LOG" >> log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
    make_tone sine
done

files=($(ls -tr *.wav))

pans_manifest=("0.00" "95.00" "10.00" "85.00" "20.00" "75.00" "30.00" "67.00" "36.00" "61.00" "42.00" "55.00")

echo "ECASOUND LOG" >> log

for ((i = 0; i < ${#files[@]}; i++)); do
	ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
	echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >>log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

