#!/bin/bash

freqs_manifest=(
    "127.09"
    "146.83"
    "151.13"
    "160.12"
    "233.08"
    "281.21"
    "306.67"
    "344.22"
    "339.29"
    "364.69"
    "409.35"
    "472.94"
    "501.07"
    "932.33"
    "959.65"
    "1002.13"
    "1046.50"
    "1077.17"
    "1157.82"
    "1837.92"
    "2834.46"
    "2959.96"
)

amps_manifest=(
  "-22" "-42" "-22" "-22" "-22" "-22" "-22" "-22"
  "-22" "-22" "-22" "-22" "-22" "-22" "-22"
"-22" "-22" "-22" "-22" "-22" "-22" "-22" )

rm log
touch log

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}


for ((i=0;i<${#freqs_manifest[@]};i++))
do
  if [ ${freqs_manifest[i]} = "146.83" ]
  then
    make_tone square
  else
    make_tone sine
  fi
done

files=($(ls -tr *.wav))

pans_manifest=(
"100.00"
"1.00"
"97.00"
"4.00"
"95.00"
"7.00"
"90.00"
"84.00"
"13.00"
"19.00"
"77.00"
"26.00"
"71.00"
"32.00"
"65.00"
"38.00"
"60.00"
"42.00"
"56.00"
"46.00"
"53.00"
"48.00"
)

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox -V panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

