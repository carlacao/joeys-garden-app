#!/bin/bash

freqs_manifest=(
	"75.57"
	"118.24"
	"130.81"
	"289.45"
	"297.94"
	"354.31"
	"472.94"
	"640.49"
	"659.26"
	"739.99"
	"1141.22"
	"1157.82"
	"1637.40"
	"2383.49"
	"2959.96"
)

amps_manifest=(
	"-22" "-22" "-22" "-22" "-22" "-22" "-22" 
	"-22" "-22" "-22" "-32" "-22" "-22"
	"-27" "-27"
)

#date|tr [:lower:] [:upper:] >> log


function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]} 
  #spectrogram -o spectrograms/`echo ${freqs_manifest[i]} | tr . -`-$1.png
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}

rm log
touch log
echo "SOX LOG" >> log

for ((i = 0; i < ${#freqs_manifest[@]}; i++)); do
	if [ ${freqs_manifest[i]} = "1141.22" ]; then
		make_tone triangle
	else
		make_tone sine
	fi
done

files=($(ls -tr *.wav))

pans_manifest=("0.00" "96.00" "6.00" "91.00" "12.00" "85.00" "18.00" "79.00" "24.00" "73.00" "30.00" "67.00" "36.00" "61.00" "42.00")

echo -e "\nECASOUND LOG" >> log

for ((i = 0; i < ${#files[@]}; i++)); do
	ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
	echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >>log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
  echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >> log
done

#tar -cvf chord1.tar converted/ && brotli -j -Z chord1.tar
