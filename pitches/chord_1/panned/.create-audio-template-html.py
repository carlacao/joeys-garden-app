from os import listdir
import glob
from os.path import isfile, join
#onlyfiles = [f for f in listdir('.') if isfile(join('.', f))]
onlyfiles = [f for f in glob.glob('*.wav') if isfile(join('.', f))]



print("""
<!doctype html> <html lang="en">

  <head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="description" content="Joey's Garden"> -->
    <!-- <meta name="Carla Cao" content="gitlab"> -->
    <title>Joey's Garden</title>

    <link rel="stylesheet" href="style.css">

  </head>

<body>
""")


for x in onlyfiles:
  print("<audio controls>" + "<source src=\"" + x + "\" type=\"audio/wav\">" + "</audio>")


print("""
</body>
</html>
""")
