SINES=( "50" "150" "320" "440" "650" "900" "2500" "3000" )
SINE_MANIFEST="${SINES[@]}"

#rm *.wav
echo "starting"

for x in $SINE_MANIFEST
do
  sox -V -r 44100 -n -b 24 -c 1 $x-hz.wav synth 75 sine $x 
done

for x in $SINE_MANIFEST
do
  ffmpeg -i $x-hz.wav -af loudnorm=I=-21:dual_mono=true:TP=-4.5:LRA=11:print_format=summary -f null -
  ffmpeg -i $x-hz.wav -af loudnorm=I=-31:TP=-1.5:LRA=11:measured_I=-37.2:measured_TP=-14.4:measured_LRA=0.1:measured_thresh=-37.7:offset=-0.5:linear=true:print_format=summary lufs-$x-hz.wav
done

#echo "inputs"

#for x in $SINE_MANIFEST
#do
#ffmpeg -i $x-hz.wav -af ebur128=framelog=verbose -f null - 2>&1 | awk '/I:/{print $2}'
#done
#
#echo "outputs"
#
#for x in $SINE_MANIFEST 
#do
#ffmpeg -i lufs-$x-hz.wav -af ebur128=framelog=verbose -f null - 2>&1 | awk '/I:/{print $2}'
#done


