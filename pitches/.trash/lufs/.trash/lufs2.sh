SINES=( "50" "150" "320" "440" "650" "900" "2500" "3000" )
SINE_MANIFEST="${SINES[@]}"


for x in $SINE_MANIFEST
do
  ffmpeg -i $x-hz.wav -af loudnorm=I=-16:dual_mono=true:TP=-1.5:LRA=11:print_format=summary -f null -
  ffmpeg -i $x-hz.wav -af ebur128=framelog=verbose -f null - 2>&1 | awk '/I:/{print $2}' 
done



# ffmpeg -i $x-hz.wav -af loudnorm=I=-16:TP=-1.5:LRA=11:measured_I=-27.2:measured_TP=-14.4:measured_LRA=0.1:measured_thresh=-37.7:offset=-0.5:linear=true:print_format=summary lufs-$x-hz.wav
