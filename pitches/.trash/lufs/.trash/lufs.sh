# for audio in "${files[@]}" 
# do
#   ffmpeg -i $audio -af loudnorm=I=-16:LRA=7:tp=-2:print_format=json -f null - 
#   ffmpeg -i $audio -af loudnorm=I=-23:LRA=7:tp=-2:measured_I=-30:measured_LRA=1.1:measured_tp=-11 04:measured_thresh=-40.21:offset=-0.47 -ar 44k -y "lufs-$audio.wav"
# done


# ffmpeg -i "${audio[0]}" -af loudnorm=I=-16:LRA=1:tp=-2:print_format=json -f null - 
# ffmpeg -i "${audio[0]}" -af loudnorm=I=-16:LRA=1:tp=-2:measured_I=-30:measured_LRA=1.1:measured_tp=-11.04:measured_thresh=-40.21:offset=-0.47 -ar 44.1k -y "lufs-${audio[0]}"

# ffmpeg -i "${audio[0]}" \
#   -af loudnorm=I=-16:dual_mono=true:TP=-1.5:LRA=11:print_format=summary \
#   -f null -


# ffmpeg -i 1500-hz.wav \
#   -af loudnorm=I=-16:dual_mono=true:TP=-1.5:LRA=11:print_format=summary \
#   -f null -


# ffmpeg -i 1500-hz.wav -af loudnorm=I=-16:TP=-1.5:LRA=11:measured_I=-27.2:measured_TP=-14.4:measured_LRA=0.1:measured_thresh=-37.7:offset=-0.5:linear=true:print_format=summary output.wav
# rm *.wav
# bash gen.sh

input=( "1500-hz.wav" "150-hz.wav" "2500-hz.wav" "3000-hz.wav" "320-hz.wav" "440-hz.wav" "50-hz.wav" "650-hz.wav" "900-hz.wav" )
output=( "lufs-1500-hz.wav" "lufs-150-hz.wav" "lufs-2500-hz.wav" "lufs-3000-hz.wav" "lufs-320-hz.wav" "lufs-440-hz.wav" "lufs-50-hz.wav" "lufs-650-hz.wav" "lufs-900-hz.wav" )

for x in "${input[@]}" 
do
  ffmpeg -i $x -af loudnorm=I=-16:dual_mono=true:TP=-1.5:LRA=11:print_format=summary -f null -
  ffmpeg -i $x -af loudnorm=I=-16:TP=-1.5:LRA=11:measured_I=-27.2:measured_TP=-14.4:measured_LRA=0.1:measured_thresh=-37.7:offset=-0.5:linear=true:print_format=summary lufs-$x
done


echo "outputs"

for x in "${output[@]}" 
do
ffmpeg -i $x -af ebur128=framelog=verbose -f null - 2>&1 | awk '/I:/{print $2}'
# ffmpeg -i $x -af ebur128=framelog=verbose -f null - 2>&1 
done


echo "inputs"

for x in "${input[@]}" 
do
ffmpeg -i $x -af ebur128=framelog=verbose -f null - 2>&1 | awk '/I:/{print $2}'
# ffmpeg -i $x -af ebur128=framelog=verbose -f null - 2>&1 
done
