#!/bin/bash

freqs_manifest=(
	"102.34" "130.81" "160.12" "223.20" "289.45" "415.30"
	"472.94" "659.26" "806.96" "1092.83" "1108.73" "1523.34" "1637.40"
	"2093.00" "2154.33" "2185.67" "2959.96" "4008.54"
)

amps_manifest=(
	"-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
	"-22" "-22" "-22" "-22" "-22" "-22" "-32"
	"-22" "-22" "-22"
)

function make_tone {
	sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
	echo "sox -V -r 44100 -n -b 16 -c 1 $(echo ${freqs_manifest[i]} | tr . -)-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >>log
}

rm log
touch log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
  if [ ${freqs_manifest[i]} = "2154.33" ]
  then
    make_tone triangle
  else
    make_tone sine
  fi
done

files=($(ls -tr *.wav))

pans_manifest=("100.00" "3.00" "94.00" "9.00" "88.00" "15.00" "82.00" "20.00" "77.00" "26.00" "71.00" "32.00" "65.00" "38.00" "59.00" "44.00" "53.00" "50.00")

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

