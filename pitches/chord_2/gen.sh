#!/bin/bash

freqs_manifest=(
  "108.42"
  "138.59"
  "160.12"
  "169.64"
  "190.42"
  "254.18"
  "364.69"
  "433.69"
  "446.40"
  "562.43"
  "587.33"
  "659.26"
  "761.67"
  "932.33"
  "987.77"
  "1046.50"
  "1157.82"
  "1244.51"
  "1523.34"
  "1891.78"
  "1947.21"
  "2249.71"
  "2793.83"
  "2959.96"
)

amps_manifest=(
  "-22" "-22" "-22" "-22" "-35" "-22" "-22" "-22"
  "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
  "-22" "-22" "-22" "-22" "-22" "-22" "-40" "-22" 
)

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}

rm log
touch log
echo -e "\n SOX LOG \n" >> log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
  if [ ${freqs_manifest[i]} = "761.67" ] || [ ${freqs_manifest[i]} = "1244.51" ] 
  then
    make_tone triangle
  elif [ ${freqs_manifest[i]} = "190.42" ] || [ ${freqs_manifest[i]} = "2793.83" ] 
  then
    make_tone square
  else
    make_tone sine
  fi
done

files=($(ls -tr *.wav))

pans_manifest=("100.00" "2.00" "96.00" "6.00" "92.00" "10.00" "88.00" "14.00" "84.00" "18.00" "80.00" "22.00" "76.00" "26.00" "72.00" "30.00" "68.00" "34.00" "64.00" "38.00" "60.00" "42.00" "56.00" "46.00")

echo -e "\n ECASOUND LOG \n" >> log

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
	sox -V panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
	echo "sox -V ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
done

