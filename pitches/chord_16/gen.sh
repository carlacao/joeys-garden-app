#!/bin/bash

freqs_manifest=(
    "75.57"
    "77.78"
    "100.87"
    "123.47"
    "273.21"
    "344.22"
    "354.31"
    "364.69"
    "421.34"
    "546.42"
    "739.99"
    "830.61"
    "1061.72"
    "1637.40"
    "1734.77"
    "2063.00"
    "2959.96"
    "3419.79"
)

amps_manifest=(
  "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
  "-22" "-22" "-42" "-22" "-22" "-22" "-22" "-32" "-32"
)

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}

rm log
touch log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
  if [ ${freqs_manifest[i]} = "830.61" ]
  then
    make_tone square
  else
    make_tone sine
  fi
done

files=($(ls -tr *.wav))

pans_manifest=("0.00" "97.00" "6.00" "91.00" "12.00" "85.00" "18.00" "79.00" "24.00" "73.00" "30.00" "67.00" "36.00" "61.00" "42.00" "56.00" "46.00" "52.00")

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done

