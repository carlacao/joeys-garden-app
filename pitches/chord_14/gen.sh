#!/bin/bash

freqs=(
    "70.30"
    "75.57"
    "130.81"
    "142.65"
    "169.64"
    "220.00"
    "433.69"
    "659.26"
    "718.92"
    "1318.51"
)

amps=(
  "-22" "-22" "-22" "-22" "-22" "-22" "-26" "-26"
  "-26" "-26" 
)

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs[i]} | tr . -`-$1.wav synth 180 $1 ${freqs[i]} fade 0.5 -0 1 gain ${amps[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs[i]} | tr . -`-$1.wav synth 180 $1 ${freqs[i]} fade 0.5 -0 1 gain ${amps[i]}" >> log
}

rm log
touch log
echo -e "SOX LOG\n"

for ((i=0;i<${#freqs[@]};i++))
do
    make_tone sine
done

files=($(ls -tr *.wav))

pans=("0.00" "94.00" "12.00" "82.00" "24.00" "70.00" "35.00" "60.00" "45.00" "50.00")

echo -e "\n ECASOUND LOG\n" >> log

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done

