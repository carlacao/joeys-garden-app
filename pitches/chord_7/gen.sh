#!/bin/bash

freqs_manifest=(
    "63.74"
    "66.36"
    "71.33"
    "82.41"
    "87.31"
    "96.59"
    "155.56"
    "160.12"
    "196.00"
    "233.08"
    "311.30"
    "375.37"
    "472.94"
    "501.07"
    "698.46"
    "739.99"
    "932.33"
    "1785.60"
    "2599.21"
    "2637.02"
    "2714.29"
    "3091.00"
    "4246.90"
)

amps_manifest=(
  "-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22"
  "-22" "-22" "-22" "-22" "-22" "-22" "-22"
"-22" "-22" "-22" "-22" "-22" "-22" "-22" "-22")

function make_tone {
  sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}
  echo "sox -V -r 44100 -n -b 16 -c 1 `echo ${freqs_manifest[i]} | tr . -`-$1.wav synth 180 $1 ${freqs_manifest[i]} fade 0.5 -0 1 gain ${amps_manifest[i]}" >> log
}

rm log
touch log
echo "SOX LOG" >> log

for ((i=0;i<${#freqs_manifest[@]};i++))
do
    make_tone sine
done

files=($(ls -tr *.wav))

pans_manifest=(
"0.00"
"88.00"
"13.00"
"85.00"
"17.00"
"82.00"
"20.00"
"78.00"
"24.00"
"74.00"
"27.00"
"71.00"
"30.00"
"68.00"
"34.00"
"64.00"
"38.00"
"60.00"
"41.00"
"58.00"
"43.00"
"55.00"
"47.00"
)

for ((i=0;i<${#files[@]};i++))
do
  ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}
  echo "ecasound -a 1 -i ${files[i]} -chcopy 1,2 -epp ${pans_manifest[i]} -f 16,2,44100 -o panned/${files[i]}" >> log
done

rm *.wav

converted_files=($(ls -tr panned/*.wav | xargs basename -s .wav))

for ((i = 0; i < ${#converted_files[@]}; i++)); do
  sox -V panned/${converted_files[i]}.wav converted/${converted_files[i]}.ogg
    echo "sox ${converted_files[i]}.wav converted/${converted_files[i]}.ogg" >>log
    done

