// Create a new Audio object for each song and attach is as a property of the element
// Also add accessibility elements
Array.prototype.forEach.call(
  document.querySelectorAll("[data-song]"),
  function (song) {
    // Add a11y attributes
    song.setAttribute("role", "button");
    song.setAttribute("aria-pressed", "false");
  }
);

// thing on the left
function reinit_tablink(element) {
    element.className = element.className.replace(" active", "");
}

function reinit_dataSong (songelement) {
    if (songelement.audio) {
        songelement.audio.pause() // XX maybe figure out how to fully stop
    }
    songelement.setAttribute("aria-pressed", "false");
}

// higher-level thing on the right
function reinit_tabcontent(divelement) {
    divelement.style.display = "none";
    Array.prototype.forEach.call(
        divelement.querySelectorAll("[data-song]"),
        reinit_dataSong
    )
}

function reinit_document(quadratNo) {
    document.title = "Quadrat " + quadratNo
}

function reinit_all(quadratNo) {
    Array.prototype.forEach.call(
        document.getElementsByClassName("tabcontent"),
        reinit_tabcontent
    )
    Array.prototype.forEach.call(
        // or
        // var x = document.getElementsByClassName("tablinks")
        // Array.from(x).forEach()
        document.getElementsByClassName("tablinks"),
        reinit_tablink
    )
    reinit_document(quadratNo)
}

function trigger_song(song) {
    // Create a new Audio object for the song, if not already done
    if (! song.audio) {
        console.log("loading "+song.href)
        song.audio = new Audio(song.href);
    }
    
    // If the item is already playing, hit pause
    if (song.getAttribute("aria-pressed") == "true") {
        song.audio.pause();
        song.setAttribute("aria-pressed", "false");
    } else {
        // Otherwise, play it
        song.audio.play();
        song.setAttribute("aria-pressed", "true");
    }
}

document.addEventListener(
    "click",
    function (event) {
        // Ignore clicks on elements that aren't the song link
        if (!event.target.hasAttribute("data-song")) return;

        // Prevent link default
        event.preventDefault();

        trigger_song(event.target)
    },
    false
);


function _openCity(element, cityNo) {
    reinit_all(cityNo)
    const cityName= "quadrat" + cityNo


  // Show the current tab, and add an "active" class to the link that opened the tab
  document.getElementById(cityName).style.display = "block";
  element.className += " active";

  // document.body.scrollTop = 0;

  document.documentElement.scrollTop = 0;
}

function openCity(evt, cityNo) {
    _openCity(evt.currentTarget, cityNo)
}

_openCity(document.getElementById("tab1"), '1')
// If you wanted to choose randomly, add id="tab.." attributes to all
// tab buttons, and generate the tab.. string randomly.

const plantNames = [
  {
    species: "Peperomia obtusifolia",
    common: "Florida peperomia, Baby rubberplant",
  },
  {
    species: "Yucca filamentosa",
    common: "Adam's needle",
  },
  {
    species: "Lasiacis divaricata",
    common: "Smallcane, Florida tibisee, Wild-bamboo",
  },
  {
    species: "Oplismenus hirtellus subsp. setarius",
    common: "Woodsgrass, Basketgrass",
  },
  {
    species: "Vitis labrusca",
    common: "Grape (cultivar – Concord)",
  },
  {
    species: "Mosiera longipes",
    common: "Longstalked-stopper",
  },
  {
    species: "Zanthoxylum fagara",
    common: "Wild-lime, Lime prickly-ash",
  },
  {
    species: "Erythrina herbacea",
    common: "Coralbean, Cherokee bean",
  },
  {
    species: "Morus alba",
    common: "White mulberry",
  },
  {
    species: "Quercus virginiana",
    common: "Virginia live oak",
  },
  {
    species: "Crossopetalum rhacoma",
    common: "Rhacoma, Maidenberry",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Ximenia americana",
    common: "Hog-plum, Tallowwood",
  },
  {
    species: "Rivina humilis",
    common: "Rougeplant, Bloodberry",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Chiococca alba",
    common: "Common snowberry",
  },
  {
    species: "Lonicera sempervirens",
    common: "Coral honeysuckle, Trumpet honeysuckle",
  },
  {
    species: "Zamia integrifolia",
    common: "Florida arrowroot, coontie",
  },
  {
    species: "Canella winterana",
    common: "Cinnamon bark, Pepper cinnamon, Wild cinnamon",
  },
  {
    species: "Encyclia tampensis",
    common: "Florida butterfly orchid",
  },
  {
    species: "Tillandsia usneoides",
    common: "Spanish-moss",
  },
  {
    species: "Dichanthelium commutatum",
    common: "Variable witchgrass",
  },
  {
    species: "Guaiacum sanctum",
    common: "Lignumvitae, Holywood lignumvitae",
  },
  {
    species: "Mimosa strigillosa",
    common: "Powderpuff, Sunshine mimosa",
  },
  {
    species: "Sophora tomentosa var. truncata",
    common: "Yellow necklacepod",
  },
  {
    species: "Crossopetalum ilicifolium",
    common: "Quailberry, Christmasberry",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Sideroxylon tenax",
    common: "Tough Florida bully",
  },
  {
    species: "Spigelia anthelmia",
    common: "West Indian pinkroot",
  },
  {
    species: "Symphyotrichum patens",
    common: "Late purple aster, clasping aster",
  },
  {
    species: "Heliotropium polyphyllum",
    common: "Pineland heliotrope",
  },
  {
    species: "Adiantum tenerum",
    common: "Brittle maidenhair",
  },
  {
    species: "Adiantum sp.",
    common: "Jumping fern.",
  },
  {
    species: "Thelypteris kunthii",
    common: "Southern shield fern",
  },
  {
    species: "Canella winterana",
    common: "Cinnamon bark, Pepper cinnamon, Wild cinnamon",
  },
  {
    species: "Sagittaria lancifolia",
    common: "Bulltongue arrowhead, lance-leaved arrowhead",
  },
  {
    species: "Sisyrinchium angustifolium",
    common: "Narrowleaf blueeyed-grass",
  },
  {
    species: "Andropogon sp.",
    common: "Unknown grass",
  },
  {
    species: "Dichanthelium commutatum",
    common: "Variable witchgrass",
  },
  {
    species: "Guaiacum sanctum",
    common: "Lignumvitae, Holywood lignumvitae",
  },
  {
    species: "Oenothera simulans",
    common: "Southern gaura, Southern beeblossum",
  },
  {
    species: "Hibiscus coccineus",
    common: "Scarlet rosemallow",
  },
  {
    species: "Stylosanthes calcicola",
    common: "Everglades key pencilflower",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Euphorbia pinetorum",
    common: "Pineland poinsettia, Pineland spurge",
  },
  {
    species: "Plumbago zeylanica",
    common:
      "Wild plumbago, Doctorbush, Florida plumbago, White plumbago, Devil’s plumbago",
  },
  {
    species: "Rivina humilis",
    common: "Rougeplant",
  },
  {
    species: "Spigelia anthelmia",
    common: "West Indian pinkroot",
  },
  {
    species: "Hamelia patens var. patens",
    common: "Firebush",
  },
  {
    species: "Bacopa monnieri",
    common: "Water hyssop, Herb-of-grace",
  },
  {
    species: "Bacopa caroliniana",
    common: "Lemon hyssop, Lemon bacopa, Blue waterhyssop",
  },
  {
    species: "Callicarpa americana",
    common: "American beautyberry",
  },
  {
    species: "Clinopodium brownei",
    common: "Browne's savory",
  },
  {
    species: "Tiedmannia filiformis",
    common: "Water dropwort, Water cowbane",
  },
  {
    species: "Commelina erecta",
    common: "Whitemouth dayflower",
  },
  {
    species: "Ananas comosus",
    common: "Pineapple",
  },
  {
    species: "Schizachyrium sp.",
    common: "Unknown grass",
  },
  {
    species: "Dichanthelium commutatum",
    common: "Variable witchgrass",
  },
  {
    species: "Oenothera simulans",
    common: "Southern gaura, Southern beeblossum",
  },
  {
    species: "Amyris elemifera",
    common: "Common torchwood, Sea torchwood",
  },
  {
    species: "Hibiscus poeppigii",
    common: "Poeppig’s rosemallow, Fairy hibiscus",
  },
  {
    species: "Vachellia farnesiana var. farnesiana",
    common: "Sweet acacia",
  },
  {
    species: "Mimosa strigillosa",
    common: "Powderpuff, Sunshine mimosa",
  },
  {
    species: "Dalea floridana",
    common: "Florida prairieclover",
  },
  {
    species: "Reynosia septentrionalis",
    common: "Darlingplum",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Euphorbia pinetorum",
    common: "Pineland poinsettia, Pineland spurge",
  },
  {
    species: "Spigelia anthelmia",
    common: "West Indian pinkroot",
  },
  {
    species: "Jacquinia keyensis",
    common: "Joewood",
  },
  {
    species: "Elephantopus elatus",
    common: "Florida elephant’s-foot, Tall elephant’s-foot",
  },
  {
    species: "Solidago fistulosa",
    common: "Pinebarren goldenrod",
  },
  {
    species: "Solidago sp.",
    common: "Showy Goldenrod",
  },
  {
    species: "Bidens alba var. radiata",
    common: "Spanish-needles",
  },
  {
    species: "Exostema caribaeum",
    common: "Caribbean princewood",
  },
  {
    species: "Ocimum campechianum",
    common: "Wild basil, Wild sweet basil",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Thelypteris kunthii",
    common: "Southern shield fern",
  },
  {
    species: "Nymphaea odorata",
    common: "American white waterlily",
  },
  {
    species: "Sagittaria lancifolia",
    common: "Bulltongue arrowhead, lance-leaved arrowhead",
  },
  {
    species: "Vallisneria americana",
    common: "Tape-grass, American eel-grass",
  },
  {
    species: "Zephyranthes simpsonii",
    common: "Rain-lily, Redmargin zephyrlily",
  },
  {
    species: "Pontederia cordata",
    common: "Pickerelweed",
  },
  {
    species: "Dichanthelium commutatum",
    common: "Variable witchgrass",
  },
  {
    species: "Hypelate trifoliata",
    common: "White-ironwood, Inkwood",
  },
  {
    species: "Colubrina arborescens",
    common: "Coffee colubrina, Greenheart",
  },
  {
    species: "Licania michauxii",
    common: "Gopher-apple",
  },
  {
    species: "Spigelia anthelmia",
    common: "West Indian pinkroot",
  },
  {
    species: "Stokesia laevis",
    common: "Stokes’ aster",
  },
  {
    species: "Gaillardia pulchella",
    common: "Blanketflower, Firewheel",
  },
  {
    species: "Coreopsis floridana",
    common: "Florida tickseed",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Lantana involucrata",
    common: "Wild-sage, Buttonsage",
  },
  {
    species: "Acrostichum danaeifolium",
    common: "Giant leather fern",
  },
  {
    species: "Pityrogramma trifoliata",
    common: "Goldenrod fern",
  },
  {
    species: "Pteridium pseudocaudatum",
    common: "Tailed bracken fern",
  },
  {
    species: "Thelypteris kunthii",
    common: "Southern shield fern",
  },
  {
    species: "Vallisneria americana",
    common: "Tape-grass, American eel-grass",
  },
  {
    species: "Crinum americanum",
    common: "Swamp-lily, Seven-sisters, String-lily",
  },
  {
    species: "Zephyranthes simpsonii",
    common: "Rain-lily, Redmargin zephyrlily",
  },
  {
    species: "Oplismenus hirtellus subsp. setarius",
    common: "Woodsgrass, Basketgrass",
  },
  {
    species: "Mosiera longpipes",
    common: "Longstalked-stopper",
  },
  {
    species: "Colubrina arborescens",
    common: "Coffee colubrina, Greenheart",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Manilkara jaimiqui subsp. emarginata",
    common: "Wild dilly",
  },
  {
    species: "Coreopsis floridana",
    common: "Florida tickseed",
  },
  {
    species: "Hamelia patens var. patens",
    common: "Firebush",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Citharexylum spinosum",
    common: "Florida fiddlewood",
  },
  {
    species: "Zamia integrifolia",
    common: "Florida arrowroot, coontie",
  },
  {
    species: "Juniperus virginiana",
    common: "Red cedar",
  },
  {
    species: "Yucca aloifolia",
    common: "Spanish-bayonet, Aloe yucca",
  },
  {
    species: "Thrinax sp.",
    common: "Thatch Palm",
  },
  {
    species: "Zanthoxylum coriaceum",
    common: "Biscayne prickly-ash",
  },
  {
    species: "Vachellia farnesiana var. farnesiana",
    common: "Sweet acacia",
  },
  {
    species: "Sophora tomentosa var. truncata",
    common: "Yellow necklacepod",
  },
  {
    species: "Dalea floridana",
    common: "Florida prairieclover",
  },
  {
    species: "Colubrina arborescens",
    common: "Coffee colubrina, Greenheart",
  },
  {
    species: "Heterosavia bahamensis",
    common: "Maidenbush, Bahama maidenbush",
  },
  {
    species: "Rivina humilis",
    common: "Rougeplant",
  },
  {
    species: "Trichocereus macrogonus var. pachanoi",
    common: "San pedro cactus",
  },
  {
    species: "Solidago sempervirens",
    common: "Seaside goldenrod",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Guettarda elliptica",
    common: "Everglades velvetseed, Hammock velvetseed",
  },
  {
    species: "Pentalinon luteum",
    common: "Wild-allamanda, Hammock viperstail",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Stachytarpheta jamaicensis",
    common: "Blue porterweed, Joee",
  },
  {
    species: "Crinum americanum",
    common: "Swamp-lily, Seven-sisters, String-lily",
  },
  {
    species: "Tillandsia sp.",
    common: "Airplant",
  },
  {
    species: "Dichanthelium commutatum",
    common: "Variable witchgrass",
  },
  {
    species: "Oplismenus hirtellus subsp. setarius",
    common: "Woodsgrass, Basketgrass",
  },
  {
    species: "Vitis labrusca",
    common: "Grape (cultivar – Concord)",
  },
  {
    species: "Bursera simaruba",
    common: "Gumbo-limbo",
  },
  {
    species: "Dalea floridana",
    common: "Florida prairieclover",
  },
  {
    species: "Colubrina elliptica",
    common: "Nakedwood, Soldierwood",
  },
  {
    species: "Ulmus americana",
    common: "American elm, Florida elm",
  },
  {
    species: "Crossopetalum ilicifolium",
    common: "Quailberry, Christmasberry",
  },
  {
    species: "Schaefferia frutescens",
    common: "Florida boxwood",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Talinum fruticosum",
    common: "Verdolaga-Francesa, Philippine-spinach, Ceylon-spinach",
  },
  {
    species: "Spigelia anthelmia",
    common: "West Indian pinkroot",
  },
  {
    species: "Stokesia laevis",
    common: "Stokes' aster",
  },
  {
    species: "Solidago fistulosa",
    common: "Pinebarren goldenrod",
  },
  {
    species: "Bidens alba var. radiata",
    common: "Spanish-needles",
  },
  {
    species: "Tagetes erecta",
    common: "Common marigold, Aztec marigold, French marigold",
  },
  {
    species: "Bourreria succulenta",
    common: "Smooth strongback, Bahama strongbark",
  },
  {
    species: "Morinda royoc",
    common: "Mouse’s pineapple, Redgal, Yellowroot, Cheese shrub",
  },
  {
    species: "Psychotria nervosa",
    common: "Shiny-leaved wild coffee",
  },
  {
    species: "Ipomoea microdactyla",
    common: "Man-in-the-ground, 'Bejuco colorado'",
  },
  {
    species: "Ocimum tenuiflorum",
    common: "Holy basil, Tulsi",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Thelypteris reticulata",
    common: "Lattice-vein fern",
  },
  {
    species: "Zamia integrifolia",
    common: "Florida arrowroot, coontie",
  },
  {
    species: "Tradescantia ohiensis",
    common: "Bluejacket, Ohio spiderwort",
  },
  {
    species: "Tillandsia usneoides",
    common: "Spanish-moss",
  },
  {
    species: "Oplismenus hirtellus subsp. setarius",
    common: "Woodsgrass, Basketgrass",
  },
  {
    species: "Myrcianthes fragrans",
    common: "Twinberry, Simpson’s stopper",
  },
  {
    species: "Colubrina elliptica",
    common: "Nakedwood, Soldierwood",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Ximenia americana",
    common: "Hog-plum, Tallowwood",
  },
  {
    species: "Verbesina virginica",
    common: "Frostweed, White crownbeard",
  },
  {
    species: "Anemia adiantifolia",
    common: "Pine fern, Maidenhair pineland fern",
  },
  {
    species: "Thelypteris kunthii",
    common: "Southern shield fern",
  },
  {
    species: "Tectaria heracleifolia",
    common: "Broad halbard fern",
  },
  {
    species: "Iris virginica",
    common: "Virginia iris, Blue flag iris",
  },
  {
    species: "Allium canadense",
    common: "Meadow garlic",
  },
  {
    species: "Coccothrinax argentata",
    common: "Florida silver palm",
  },
  {
    species: "Oplismenus hirtellus subsp. setarius",
    common: "Woodsgrass, Basketgrass",
  },
  {
    species: "Eugenia axillaris",
    common: "White stopper",
  },
  {
    species: "Senna mexicana var. chapmanii",
    common: "Bahama senna, Chapman's wild sensitive plant",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Heterosavia bahamensis",
    common: "Maidenbush, Bahama maidenbush",
  },
  {
    species: "Salix caroliniana",
    common: "Coastal Plain willow, Carolina willow",
  },
  {
    species: "Opuntia ficus-indica",
    common: "Nopal",
  },
  {
    species: "Asclepias curassavica",
    common: "Scarlet milkweed, Bloodflower",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Trichostema dichotomum",
    common: "Forked bluecurls",
  },
  {
    species: "Scutellaria havanensis",
    common: "Havana skullcap",
  },
  {
    species: "Zamia integrifolia",
    common: "Florida arrowroot, coontie",
  },
  {
    species: "Pseudophoenix sargentii",
    common: "Sargent's palm, Sargent's cherry palm, Buccaneer palm",
  },
  {
    species: "Tradescantia ohiensis",
    common: "Bluejacket, Ohio spiderwort",
  },
  {
    species: "Herissantia crispa",
    common: "Bladdermallow",
  },
  {
    species: "Gossypium hirsutum",
    common: "Wild cotton, Upland cotton",
  },
  {
    species: "Sophora tomentosa var. truncata",
    common: "Yellow necklacepod",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Chrysobalanus icaco",
    common: "Coco-plum",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Rivina humilis",
    common: "Rougeplant, Bloodberry",
  },
  {
    species: "Gaillardia pulchella",
    common: "Blanketflower, Firewheel",
  },
  {
    species: "Bidens alba var. radiata",
    common: "Spanish-needles",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Jacquemontia pentanthos",
    common: "Skyblue clustervine",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Agave decipiens",
    common: "False-sisal",
  },
  {
    species: "Tradescantia ohiensis",
    common: "Bluejacket, Ohio spiderwort",
  },
  {
    species: "Dichanthelium commutatum",
    common: "Variable witchgrass",
  },
  {
    species: "Eugenia confusa",
    common: "Ironwood, Redberry stopper",
  },
  {
    species: "Herissantia crispa",
    common: "bladdermallow",
  },
  {
    species: "Krugiodendron ferreum",
    common: "Black ironwood",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Opuntia humifusa",
    common: "Pricklypear",
  },
  {
    species: "Solidago sp.",
    common: "Showy Goldenrod",
  },
  {
    species: "Erigeron quercifolius",
    common: "Southern-fleabane, Oakleaf fleabane",
  },
  {
    species: "Bourreria succulenta",
    common: "Smooth strongback, Bahama strongbark",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Echites umbellatus",
    common: "Devil’s-potato, Rubbervine",
  },
  {
    species: "Asclepias curassavica",
    common: "Scarlet milkweed, Bloodflower",
  },
  {
    species: "Asclepias incarnata",
    common: "Swamp milkweed",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Ruellia succulenta",
    common: "Thickleaf wild petunia",
  },
  {
    species: "Agave tequilana",
    common: "Blue agave",
  },
  {
    species: "Oplismenus hirtellus subsp. setarius",
    common: "Woodsgrass, Basketgrass",
  },
  {
    species: "Conocarpus erectus",
    common: "Silver buttonwood",
  },
  {
    species: "Herissantia crispa",
    common: "Bladdermallow",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Byrsonima lucida",
    common: "Locustberry",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Plumbago zeylanica",
    common:
      "Wild plumbago, Doctorbush, Florida plumbago, White plumbago, Devil’s plumbago",
  },
  {
    species: "Rivina humilis",
    common: "Rougeplant, Bloodberry",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Vallesia antillana",
    common: "Pearlberry, Tearshrub",
  },
  {
    species: "Salvia misella",
    common: "Southern river sage, Creeping sage",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Lantana involucrata",
    common: "Wild-sage, Buttonsage",
  },
  {
    species: "Agave decipiens",
    common: "False-sisal",
  },
  {
    species: "Myrcianthes fragrans",
    common: "Twinberry, Simpson’s stopper",
  },
  {
    species: "Vachellia farnesiana var. farnesiana",
    common: "Sweet acacia",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Rivina humilis",
    common: "Rougeplant, Bloodberry",
  },
  {
    species: "Consolea corallicola",
    common: "Semaphore pricklypear",
  },
  {
    species: "Gelsemium sempervirens",
    common: "Yellow jessamine, Carolina jessamine",
  },
  {
    species: "Forestiera segregata",
    common: "Florida privet, Florida swampprivet",
  },
  {
    species: "Salvia misella",
    common: "Southern river sage, Creeping sage",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Eragrostis elliottii",
    common: "Elliott’s love grass",
  },
  {
    species: "Muhlenbergia capillaris",
    common: "Muhlygrass, Hairawn muhly",
  },
  {
    species: "Mimosa strigillosa",
    common: "Powderpuff, Sunshine mimosa",
  },
  {
    species: "Galactia volubilis",
    common: "Downy milkpea",
  },
  {
    species: "Coreopsis leavenworthii",
    common: "Leavenworth’s tickseed",
  },
  {
    species: "Cordia sebestena",
    common: "Orange geigertree, Largeleaf geigertree",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Echites umbellatus",
    common: "Devil’s-potato, Rubbervine",
  },
  {
    species: "Ipomoea imperati",
    common: "Beach morningglory",
  },
  {
    species: "Ipomoea pes-caprae subsp. brasiliensis",
    common: "Railroad vine, Bayhops",
  },
  {
    species: "Phyla stoechadifolia",
    common: "Southern fogfruit",
  },
  {
    species: "Lantana depressa var. depressa",
    common: "Pineland lantana, Rockland shrubverbena",
  },
  {
    species: "Tetrazygia bicolor",
    common: "West Indian-lilac, Florida clover ash",
  },
  {
    species: "Senna mexicana var. chapmanii",
    common: "Bahama senna, Chapman's wild sensitive plant",
  },
  {
    species: "Sideroxylon tenax",
    common: "Tough Florida bully",
  },
  {
    species: "Heliotropium polyphyllum",
    common: "Pineland heliotrope",
  },
  {
    species: "Physalis walteri",
    common: "Walter’s groundcherry",
  },
  {
    species: "Monarda punctata",
    common: "Horsemint, Spotted beebalm",
  },
  {
    species: "Eustachys petraea",
    common: "Common fingergrass, Pinewoods fingergrass",
  },
  {
    species: "Mangifera indica",
    common: "Mango",
  },
  {
    species: "Mimosa strigillosa",
    common: "Powderpuff, Sunshine mimosa",
  },
  {
    species: "Colubrina arborescens",
    common: "Coffee colubrina, Greenheart",
  },
  {
    species: "Colubrina elliptica",
    common: "Nakedwood, Soldierwood",
  },
  {
    species: "Pilea microphylla",
    common: "Artillery fern, Artillery plant, Rockweed",
  },
  {
    species: "Croton linearis",
    common: "Pineland croton, Grannybush",
  },
  {
    species: "Passiflora suberosa",
    common: "Corkystem passionflower",
  },
  {
    species: "Coreopsis leavenworthii",
    common: "Leavenworth’s tickseed",
  },
  {
    species: "Echinacea purpurea",
    common: "Eastern Purple Coneflower, Purple Coneflower",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Asclepias tuberosa",
    common: "Butterflyweed, Butterfly milkweed",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Origanum vulgare",
    common: "Oregano",
  },
  {
    species: "Origanum majorana",
    common: "Marjoram (Sweet marjoram, Knotted marjoram)",
  },
  {
    species: "Aloysia virgata",
    common: "Sweet almond verbena, Sweet almond bush",
  },
  {
    species: "Tillandsia sp.",
    common: "Airplant",
  },
  {
    species: "Oplismenus hirtellus subsp. setarius",
    common: "Woodsgrass, Basketgrass",
  },
  {
    species: "Terminalia molinetii",
    common: "Spiny black-olive",
  },
  {
    species: "Quadrella cynophallophora",
    common: "Jamaica caper-tree",
  },
  {
    species: "Mimosa strigillosa",
    common: "Powderpuff, Sunshine mimosa",
  },
  {
    species: "Argythamnia blodgettii",
    common: "Blodgett’s wild mercury, Blodgett’s silverbush",
  },
  {
    species: "Portulaca oleracea",
    common: "Purslane, Little hogweed",
  },
  {
    species: "Spigelia anthelmia",
    common: "West Indian pinkroot",
  },
  {
    species: "Bidens alba var. radiata",
    common: "Spanish-needles",
  },
  {
    species: "Berlandiera subacaulis",
    common: "Florida green-eyes",
  },
  {
    species: "Ambrosia hispida",
    common: "Beach ragweed, Coastal ragweed",
  },
  {
    species: "Liatris spicata",
    common: "Dense gayfeather, Blazing star",
  },
  {
    species: "Tournefortia gnaphalodes",
    common: "Sea-lavender, Sea-rosemary",
  },
  {
    species: "Heliotropium angiospermum",
    common: "Scorpionstail",
  },
  {
    species: "Asclepias curassavica",
    common: "Scarlet milkweed, Bloodflower",
  },
  {
    species: "Salvia coccinea",
    common: "Tropical sage, Scarlet sage, Blood sage",
  },
  {
    species: "Stachytarpheta jamaicensis",
    common: "Blue porterweed, Joee",
  },
];

function plantToggle(elem) {
  console.log(elem.innerText);

  let plantName = elem.innerText;
  let commonNames = plantNames.filter((plant) => plant.species === plantName);
  let latinNames = plantNames.filter((plant) => plant.common === plantName);
  if (commonNames.length) {
    elem.innerText = commonNames[0].common;
    console.log(elem.innerText);
  }

  if (latinNames.length) {
    elem.innerText = latinNames[0].species;
    console.log(elem.innerText);
  }
}

function myClickHandler(e) {
  console.log(e.target.tagName);
  if (e.target.tagName === "A") {
    plantToggle(e.target);
  }
}

document.addEventListener("click", myClickHandler);
